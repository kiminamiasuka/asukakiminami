package chapter6.service;

import static chapter6.utils.CloseableUtil.*;
import static chapter6.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import bulletinBoard.beans.CustomPost;
import bulletinBoard.beans.Post;
import bulletinBoard.beans.Search;
import bulletinBoard.dao.PostDao;

public class PostService {

	//Post入力用
	public void register(Post post) {
		Connection connection = null; //Connection=接続のコンテキスト内でSQL文が実行され結果が返されます
		try {
			connection = getConnection();
			PostDao postDao = new PostDao();
			postDao.insert(connection, post);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	
	public List<Post> findAll() {
		Connection connection = null;
		try {
			connection = getConnection();
			PostDao postDao = new PostDao();
			List<Post> postList = postDao.findAll(connection);
			commit(connection);
			return postList;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	
	public CustomPost getCustomPost(int postsId) {
		Connection connection = null;
		try {
			connection = getConnection();
			PostDao postDao = new PostDao();
			CustomPost customPost = postDao.getCustomPost(connection, postsId);
			commit(connection);
			return customPost;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	//投稿のすべてを表示するときに使用
	public List<CustomPost> customPostAll() {
		Connection connection = null;
		try {
			connection = getConnection();
			PostDao postDao = new PostDao();
			List<CustomPost> customPostList = postDao.customPostAll(connection);
			commit(connection);
			return customPostList;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	//検索用Service
	public List<CustomPost>search(Search search){
		Connection connection = null;
		try {
			connection = getConnection();
			PostDao postDao = new PostDao();
			List<CustomPost> customPostList = postDao.search(connection,search);
			commit(connection);
			return customPostList;
		}catch(RuntimeException e) {
			rollback(connection);
			throw e;
		}catch(Error e) {
			rollback(connection);
			throw e;
		}finally {
			close(connection);
		}
	}
	

	//Postのdelete用
	public void delete(int id) {
		// TODO 自動生成されたメソッド・スタブ
		Connection connection = null;
		try {
			connection = getConnection();
			PostDao postDao = new PostDao();
			postDao.delete(connection, id);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}


	
}