package chapter6.service;

import static chapter6.utils.CloseableUtil.*;
import static chapter6.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import bulletinBoard.beans.Comment;
import bulletinBoard.dao.CommentDao;

public class CommentService {
	//コメント機能で使用
	public void register(Comment comment) {

        Connection connection = null;  //Connection=接続のコンテキスト内でSQL文が実行され結果が返されます
        try {
            connection = getConnection();

            CommentDao CommentDao = new CommentDao();
            CommentDao.insert(connection, comment);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

	//Commentのdelete用
    public void delete(int id) {
    	Connection connection = null;
    	try {
    		connection = getConnection();
    		CommentDao commentDao = new CommentDao();
    		commentDao.delete(connection, id);
    		commit(connection);
    	}catch(RuntimeException e) {
    		rollback(connection);
    		throw e;
    	}catch(Error e) {
    		rollback(connection);
    		throw e;
    	}finally {
    		close(connection);
    	}
    }

  //コメントのすべてを表示するときに使用
	public List<Comment> commentAll() {
		// TODO 自動生成されたメソッド・スタブ
		Connection connection = null;
		try {
			connection = getConnection();
			CommentDao commentDao = new CommentDao();
			List<Comment> commentList = commentDao.commentAll(connection);
			commit(connection);
			return commentList;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	
}
