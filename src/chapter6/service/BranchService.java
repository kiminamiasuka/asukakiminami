package chapter6.service;

import static chapter6.utils.CloseableUtil.*;
import static chapter6.utils.DBUtil.*;

import java.sql.Connection;
import java.util.Map;

import bulletinBoard.dao.BranchDao;

public class BranchService {
	
	//branchのすべての情報
	public Map<String, String> findAll(){
		Connection connection = null;
		try {
			connection = getConnection();
			BranchDao branchDao = new BranchDao();
			Map<String, String> branchMap = branchDao.getBranch(connection);
			commit(connection);
			return branchMap;
		}catch (RuntimeException e) {
			rollback(connection);
			throw e;
		}catch (Error e) {
			rollback(connection);
			throw e;
		}finally {
			close(connection);
		}
	}

}
