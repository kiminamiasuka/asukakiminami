package chapter6.service;

import static chapter6.utils.CloseableUtil.*;
import static chapter6.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import bulletinBoard.beans.ComplementUser;
import bulletinBoard.beans.User;
import bulletinBoard.dao.UserDao;
import chapter6.utils.CipherUtil;

public class UserService {

	public void register(User user) {

		Connection connection = null;
		try {
			connection = getConnection();

			String encPassword = CipherUtil.encrypt(user.getPassword());
			user.setPassword(encPassword);

			UserDao userDao = new UserDao();
			userDao.insert(connection, user);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public boolean duplicateCheck(User user) {
		// TODO 自動生成されたメソッド・スタブ
		Connection connection = null;
		try {
			connection = getConnection();
			UserDao userDao = new UserDao();
			boolean bool = userDao.duplicateCheck(connection, user);
			commit(connection);
			return bool;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public void setStatus(User user) {
		// TODO 自動生成されたメソッド・スタブ
		Connection connection = null;
		try {
			connection = getConnection();
			UserDao userDao = new UserDao();
			userDao.setStatus(connection, user);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}

	}

	//EditUserでIDを所得
	public User getUser(int userId) {

		Connection connection = null;
		try {
			connection = getConnection();

			UserDao userDao = new UserDao();
			User user = userDao.getUser(connection, userId);

			commit(connection);

			return user;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public List<ComplementUser> getAllComplementUser() {
		// TODO 自動生成されたメソッド・スタブ
		Connection connection = null;
		try {
			connection = getConnection();
			UserDao userDao = new UserDao();
			List<ComplementUser> userList = userDao.getAiiComplementUser(connection);
			commit(connection);
			return userList;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	//EditUserで使用
	public void update(User user) {

		Connection connection = null;
		try {
			connection = getConnection();

			// passwordに値がある場合
			if (!user.getPassword().equals("")) {
				String encPassword = CipherUtil.encrypt(user.getPassword());
				user.setPassword(encPassword);
			}

			UserDao userDao = new UserDao();
			userDao.update(connection, user);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public List<Integer> getUserIdList(int branchId) {
		Connection connection = null;
		try {
			connection = getConnection();

			UserDao userDao = new UserDao();
			List<Integer> userIdList = userDao.getUserIdList(connection, branchId);
			commit(connection);
			return userIdList;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

}