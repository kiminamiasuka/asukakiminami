package bulletinBoard.beans;

import java.io.Serializable;
import java.util.Date;

public class User implements Serializable { //クラスを定義する際にimplememntsキーワードを付けて実装元を指定
	private static final long serialVersionUID = 1L; //バイト配列化し、インスタンス情報をハードディスクやネットワークに送受信できる
	//User用のbeans
	private int id; //変数の型指定と定義
	private String loginId;
	private String name;
	private String password;
	private int branchId;
	private int departmentId;
	private boolean isStopped;
	private Date createdDate;
	private Date updatedDate;

	public int getId() { //セッションの値を取得するメソッド
		return id; //値を持ってreturnで返す
	}

	public void setId(int id) { //値をセットするメソッド
		this.id = id; //this.値(上の値)に値をセットする
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isStopped() {
		return isStopped;
	}

	public void setStopped(boolean isStopped) {
		this.isStopped = isStopped;
	}

	public int getBranchId() {
		return branchId;
	}

	public void setBranchId(int branchId) {
		this.branchId = branchId;
	}

	public int getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(int departmentId) {
		this.departmentId = departmentId;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
}