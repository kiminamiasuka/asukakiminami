package bulletinBoard.beans;

import java.io.Serializable;
import java.util.Date;


public class Post implements Serializable {
	private static final long serialVersionUID = 1L; 
	//Message用のbeans
	private int id; //idを型変数に宣言
	private int userId;
	private String text;
	private String subject;
	private String category;
	private Date createdDate;
	private Date updatedDate;

	public int getId() { //セッションの値を取得するメソッド
		return id; //値を持ってreturnで返す
	}

	public void setId(int id) { //値をセットするメソッド
		this.id = id; //this.値(上の値)に値をセットする
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

}