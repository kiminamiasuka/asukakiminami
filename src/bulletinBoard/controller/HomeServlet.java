package bulletinBoard.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import bulletinBoard.beans.Comment;
import bulletinBoard.beans.CustomPost;
import bulletinBoard.beans.Search;
import bulletinBoard.beans.User;
import chapter6.service.CommentService;
import chapter6.service.PostService;

@WebServlet(urlPatterns = { "/index.jsp" })
public class HomeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	//home画面に要素を送る
	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		boolean from = true;
		boolean to = true;
		Search search = setInputSearch(request);
		List<String> messages = new ArrayList<String>();

		//ユーザー管理へのリンクを表示させるか否か
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("loginUser");
		if (user.getDepartmentId() == 1) {
			request.setAttribute("isManager", true);
		}

		//検索が初期値の時
		if (search.getCategory() == null && search.getFrom() == null && search.getTo() == null) {
			List<CustomPost> customPostList = new PostService().customPostAll();
			List<Comment> commentList = new CommentService().commentAll();

			request.setAttribute("loginUser", session.getAttribute("loginUser"));
			request.setAttribute("customPostList", customPostList);
			request.setAttribute("commentList", commentList);
			request.getRequestDispatcher("home.jsp").forward(request, response);
		} else {
			
			//検索機能
			if (!StringUtils.isBlank(search.getCategory())) {
				request.setAttribute("editCategory", search.getCategory());
			}
			if (!StringUtils.isBlank(search.getFrom())) {
				request.setAttribute("editFrom", search.getFrom());
				from = checkDate(request, search.getFrom());
			}
			if (!StringUtils.isBlank(search.getTo())) {
				request.setAttribute("editTo", search.getTo());
				to = checkDate(request, search.getTo());
			}

			//from or to に異常がある場合
			if (!(from && to)) {
				messages.add("検索機能に異常があります");
				request.setAttribute("errorMessages", messages);
				request.getRequestDispatcher("home.jsp").forward(request, response);
				return;
			}

			//検索結果がない場合
			List<CustomPost> customPostList = new PostService().search(search);
			if (customPostList == null) {
				messages.add("検索結果はありません");
				request.setAttribute("errorMessages", messages);
			}
			request.setAttribute("customPostList", customPostList);

			request.getRequestDispatcher("home.jsp").forward(request, response);
		}
	}

	//日付のチェック
	private boolean checkDate(HttpServletRequest request, String inputDate) {
		// TODO 自動生成されたメソッド・スタブ
		return inputDate.matches("[0-9]{4}-[0-9]{2}-[0-9]{2}");
	}

	//検索の入力
	private Search setInputSearch(HttpServletRequest request) {
		// TODO 自動生成されたメソッド・スタブ
		Search search = new Search();
		search.setCategory(request.getParameter("category"));
		search.setFrom(request.getParameter("from"));
		search.setTo(request.getParameter("to"));
		return search;
	}

	//コメント機能
	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		List<String> messages = new ArrayList<String>();
		Comment comment = setInputComment(request);
		if (isValid(request, messages)) {
			new CommentService().register(comment);
			response.sendRedirect("./");
		} else {
			HttpSession session = request.getSession();
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("./");
		}
	}

	//コメントをセット
	private Comment setInputComment(HttpServletRequest request) {
		// TODO 自動生成されたメソッド・スタブ
		Comment comment = new Comment();
		comment.setPostsId(Integer.parseInt(request.getParameter("posts_id")));
		comment.setUserId(Integer.parseInt(request.getParameter("user_id")));
		comment.setComment(request.getParameter("comment"));
		return comment;
	}

	//コメントのエラー処理
	private boolean isValid(HttpServletRequest request, List<String> messages) {
		// TODO 自動生成されたメソッド・スタブ
		String comment = request.getParameter("comment");

		if (StringUtils.isBlank(comment)) {
			messages.add("コメントを入力してください");
		} else if (comment.length() > 500) {
			messages.add("コメントは500文字以下で入力してください");
		}
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}
