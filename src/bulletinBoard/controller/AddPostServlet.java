package bulletinBoard.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import bulletinBoard.beans.Post;
import bulletinBoard.beans.User;
import chapter6.service.PostService;

@WebServlet(urlPatterns = { "/post" }) 
public class AddPostServlet extends HttpServlet { 
	private static final long serialVersionUID = 1L; 

	//投稿画面表示
	@Override
	protected void doGet(HttpServletRequest request, 
			HttpServletResponse response) throws IOException, ServletException { 
		HttpSession session = request.getSession();
		User status = (User) session.getAttribute("loginUser");
		request.setAttribute("userId", status.getId());
		request.getRequestDispatcher("post.jsp").forward(request, response); 
	}

	//メッセージをjspで表示
	@Override 
	protected void doPost(HttpServletRequest request, 
			HttpServletResponse response) throws IOException, ServletException { 

		List<String> messages = new ArrayList<String>();
		Post post = setInputPost(request);
		if (isValid(request, messages)) {
			new PostService().register(post);
			response.sendRedirect("./");
		} else {
			request.setAttribute("errorMessages", messages);
			request.setAttribute("editPost", post);
			request.setAttribute("userId", post.getUserId());
			request.getRequestDispatcher("post.jsp").forward(request, response);
		}
	}

	//ｊｓｐの情報をParameterで持ってくる
	private Post setInputPost(HttpServletRequest request) {
		// TODO 自動生成されたメソッド・スタブ
		Post post = new Post();
		post.setSubject(request.getParameter("subject"));
		post.setText(request.getParameter("text"));
		post.setCategory(request.getParameter("category"));
		post.setUserId(Integer.parseInt(request.getParameter("user_id")));

		return post;
	}

	//エラー処理
	private boolean isValid(HttpServletRequest request, List<String> messages) {

		String subject = request.getParameter("subject");
		String post = request.getParameter("text"); //messageをキーに値を持ってきてmessageに代入
		String category = request.getParameter("category");

		if (StringUtils.isBlank(subject) == true) {
			messages.add("件名を入力してください");
		} else if (subject.length() > 30) {
			messages.add("件名を30文字以下で入力してください");
		}

		if (StringUtils.isBlank(post) == true) { //messageの中身がない時の処理
			messages.add("メッセージを入力してください");
		}
		if (post.length() > 1000) { //message.length(要素の数)が140を超えたときの処理
			messages.add("メッセージは1000文字以下で入力してください");
		}
		if (StringUtils.isBlank(category)) {
			messages.add("カテゴリーを入力してください");
		} else if (category.length() > 10) {
			messages.add("カテゴリーは10文字以下で入力してください");
		}
		if (messages.size() == 0) { //messages.sizeが0の時
			return true;
		} else {
			return false;
		}
	}
}