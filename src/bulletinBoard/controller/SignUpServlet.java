package bulletinBoard.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import bulletinBoard.beans.User;
import chapter6.service.BranchService;
import chapter6.service.DepartmentService;
import chapter6.service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override //新規登録画面に要素を送る
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();
		Map<String, String> branchMap = new BranchService().findAll();
		Map<String, String> departmentMap = new DepartmentService().findAll();
		session.setAttribute("branch", branchMap);
		session.setAttribute("department", departmentMap);

		request.getRequestDispatcher("signup.jsp").forward(request, response);
	}

	//情報を登録
	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();
		List<String> messages = new ArrayList<String>();
		User user = setInputUser(request);

		if (isValid(request, messages)) {
			UserService userService = new UserService();
			//ログインIDの重複チェック
			if (!userService.duplicateCheck(user)) {
				messages.add("そのログインIDは既に使用されています");
				request.setAttribute("errorMessages", messages);
				request.getRequestDispatcher("signup.jsp").forward(request, response);
				return;
			}

			//登録
			userService.register(user);
			session.removeAttribute("branch");
			session.removeAttribute("department");
			response.sendRedirect("./");
		} else {
			request.setAttribute("errorMessages", messages);
			request.setAttribute("editUser", user);
			request.getRequestDispatcher("signup.jsp").forward(request, response);
		}
	}

	private User setInputUser(HttpServletRequest request) {

		User user = new User(); 
		user.setName(request.getParameter("name")); 
		user.setLoginId(request.getParameter("login_id"));
		user.setPassword(request.getParameter("password"));
		user.setBranchId(Integer.parseInt(request.getParameter("branch_id")));
		user.setDepartmentId(Integer.parseInt(request.getParameter("department_id")));

		return user;
	}

	//設定時にアカウント名やパスワードが入っていないときのエラーメッセージ処理
	private boolean isValid(HttpServletRequest request, List<String> messages) {
		String loginId = request.getParameter("login_id"); 
		String password = request.getParameter("password");
		String passwordConfirm = request.getParameter("password_confirm");
		String name = request.getParameter("name");
		String branchId = request.getParameter("branch_id");
		String departmentId = request.getParameter("department_id");
		boolean passwordFlag = false;
		boolean passwordConfirmFlag = false;

		if (StringUtils.isBlank(loginId) == true) { 
			messages.add("ログインIDを入力してください");
		} else if (!loginId.matches("[a-zA-Z0-9]+")) {
			messages.add("ログインIDは半角英数字で入力してください");
		} else if(loginId.length() < 6) {
			messages.add("ログインIDは6文字以上で入力してください");
		} else if(loginId.length() >= 20) {
			messages.add("ログインIDは20文字以下で入力してください");
		}
		
		if (StringUtils.isBlank(password) == true) {
			messages.add("パスワードを入力してください");
		} else if (!password.matches("[ -/:-@\\[-~]+")) {
			messages.add("パスワードは半角英数字、記号で入力してください");
		} else if(password.length() < 6) {
			messages.add("パスワードは6文字以上で入力してください");
		} else if(password.length() >= 20) {
			messages.add("パスワードは20文字以下で入力してください");
		} else {
			passwordFlag = true;
		}
		
		if (StringUtils.isBlank(password)) {
			messages.add("確認用パスワードを入力してください");
		} else if (!password.matches("[ -/:-@\\[-~]{6,20}")) {
			messages.add("確認用パスワードは半角英数字、記号で入力してください");
		} else {
			passwordConfirmFlag = true;
		}
		if (passwordFlag && passwordConfirmFlag) {
			if (!password.equals(passwordConfirm)) {
				messages.add("パスワードと確認用パスワードが一致していません");
			}
		}
		
		if (StringUtils.isBlank(name)) {
			messages.add("名前を入力してください");
		} else if (name.length() > 10) {
			messages.add("名前は10文字以下で入力してください");
		}
		
		if (StringUtils.isBlank(branchId) == true) {
			messages.add("部署IDを入力してください");
		} else if (branchId.equals("1")) {
			if (!(departmentId.equals("1") || departmentId.equals("2"))) {
				messages.add("支店と部署・役職の組み合わせが不正です");
			}
		} else if (!branchId.equals("1")) {
			if (departmentId.equals("1") || departmentId.equals("2")) {
				messages.add("支店と部署・役職の組み合わせが不正です");
			}
		}
		
		if (StringUtils.isBlank(departmentId)) {
			messages.add("部署を指定してください");
		}

		// TODO アカウントが既に利用されていないか、メールアドレスが既に登録されていないかなどの確認も必要
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

}