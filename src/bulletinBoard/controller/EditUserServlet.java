package bulletinBoard.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import bulletinBoard.beans.User;
import chapter6.service.BranchService;
import chapter6.service.DepartmentService;
import chapter6.service.UserService;

@WebServlet(urlPatterns = { "/editUser" })
public class EditUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();
		String id = request.getParameter("id");
		User status = (User) session.getAttribute("loginUser");

		Map<String, String> branchMap = new BranchService().findAll();
		Map<String, String> departmentMap = new DepartmentService().findAll();
		session.setAttribute("branch", branchMap);
		session.setAttribute("department", departmentMap);

		int userId = 0;
		userId = Integer.parseInt(id);
		User user = new UserService().getUser(userId);

		//データをJSPに表示
		session.setAttribute("userId", id);
		request.setAttribute("editUser", user);
		request.setAttribute("myId", status.getId());
		request.setAttribute("user", status);
		request.getRequestDispatcher("editUser.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		HttpSession session = request.getSession();
		List<String> messages = new ArrayList<String>();
		User user = setInputUser(request);
		UserService userService = new UserService();

		if (isValid(request, messages)) {

			//updateを行いsession内のdateを削除
			userService.update(user);
			session.removeAttribute("branch"); //removeAttribute = セッションオブジェクトの削除
			session.removeAttribute("department");
			session.removeAttribute("userId");
			response.sendRedirect("./userManagement");
		} else {
			request.setAttribute("errorMessages", messages);
			request.setAttribute("editUser", user);
			request.getRequestDispatcher("editUser.jsp").forward(request, response);
		}

	}

	//JSPの情報をセット
	private User setInputUser(HttpServletRequest request) {
		// TODO 自動生成されたメソッド・スタブ
		User user = new User();
		user.setId(Integer.parseInt(request.getParameter("id")));
		user.setLoginId(request.getParameter("login_id"));
		user.setPassword(request.getParameter("password"));
		user.setName(request.getParameter("name"));
		user.setBranchId(Integer.parseInt(request.getParameter("branch_id")));
		user.setDepartmentId(Integer.parseInt(request.getParameter("department_id")));

		return user;
	}

	//エラー処理
	private boolean isValid(HttpServletRequest request, List<String> messages) {
		// TODO 自動生成されたメソッド・スタブ

		String loginId = request.getParameter("login_id");
		String password = request.getParameter("password");
		String passwordConfirm = request.getParameter("password_confirm");
		String name = request.getParameter("name");
		String branchId = request.getParameter("branch_id");
		String departmentId = request.getParameter("department_id");
		boolean passwordFlag = false;
		boolean passwordConfirmFlag = false;

		if (StringUtils.isBlank(loginId) == true) {
			messages.add("ログインIDを入力してください");
		} else if (!loginId.matches("[a-zA-Z0-9]+")) {
			messages.add("ログインIDは半角英数字で入力してください");
		} else if (loginId.length() < 6) {
			messages.add("ログインIDは6文字以上で入力してください");
		} else if (loginId.length() >= 20) {
			messages.add("ログインIDは20文字以下で入力してください");
		}

		if (!StringUtils.isBlank(password) == true) {
			if (!password.matches("[ -/:-@\\[-~]+")) {
				messages.add("パスワードは半角英数字、記号で入力してください");
			} else if (password.length() < 6) {
				messages.add("パスワードは6文字以上で入力してください");
			} else if (password.length() >= 20) {
				messages.add("パスワードは20文字以下で入力してください");
			} else {
				passwordFlag = true;
			}
		}
		if (passwordFlag) {
			if (StringUtils.isBlank(passwordConfirm) == true) {
				messages.add("確認用パスワードを入力してください");
			} else if (!passwordConfirm.matches("[ -/:-@\\[-~]{6,20}")) {
				messages.add("確認用パスワードは半角英数字、記号で6文字以上20文字以内で入力してください");
			} else {
				passwordConfirmFlag = true;
			}
		}

		if (passwordFlag && passwordConfirmFlag) {
			if (!password.equals(passwordConfirm)) {
				messages.add("パスワードと確認用パスワードが違います");
			}
		}

		if (StringUtils.isBlank(name) == true) {
			messages.add("名前を入力してください");
		} else if (name.length() > 10) {
			messages.add("名前は10文字以内で入力してください");
		}

		if (StringUtils.isBlank(branchId) == true) {
			messages.add("部署IDを入力してください");
		} else if (branchId.equals("1")) {
			if (!(departmentId.equals("1") || departmentId.equals("2"))) {
				messages.add("支店と部署・役職の組み合わせが不正です");
			}
		} else if (!branchId.equals("1")) {
			if (departmentId.equals("1") || departmentId.equals("2")) {
				messages.add("支店と部署・役職の組み合わせが不正です");
			}
		}

		if (StringUtils.isBlank(departmentId) == true) {
			messages.add("役職IDを入力してください");
		}

		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}
