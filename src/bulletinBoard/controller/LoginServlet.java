package bulletinBoard.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import bulletinBoard.beans.User;
import chapter6.service.LoginService;

@WebServlet(urlPatterns = { "/login" }) 
public class LoginServlet extends HttpServlet { 
	private static final long serialVersionUID = 1L;
	
	//login画面に要素を送る
	@Override
	protected void doGet(HttpServletRequest request, 
			HttpServletResponse response) throws IOException, ServletException { 
		
		request.getRequestDispatcher("login.jsp").forward(request, response); 
	}

	//loginの値を生成
	@Override
	protected void doPost(HttpServletRequest request, 
			HttpServletResponse response) throws IOException, ServletException { 

		String loginId = request.getParameter("login_id"); 
		String password = request.getParameter("password"); 
		List<String> messages = new ArrayList<String>();

		if (isValid(request, messages)) {
			User user = new LoginService().login(loginId, password);
			HttpSession session = request.getSession(); 

			if (user != null) { 				
				if(user.isStopped() == true) {
					messages.add("ログインIDまたはパスワードが誤っています");
					request.setAttribute("errorMessages", messages); 
					request.getRequestDispatcher("login.jsp").forward(request, response);
				}
				
				session.setAttribute("loginUser", user); 
				response.sendRedirect("./");
			} else { 
				messages.add("ログインIDまたはパスワードが誤っています"); 
				request.setAttribute("errorMessages", messages); 
				request.setAttribute("editLogin", loginId);
				request.getRequestDispatcher("login.jsp").forward(request, response);
			}
		} else {
			request.setAttribute("errorMessages", messages);
			request.setAttribute("editLogin", loginId);
			request.getRequestDispatcher("login.jsp").forward(request, response);
		}
	}

	//エラー処理
	private boolean isValid(HttpServletRequest request, List<String> messages) {
		// TODO 自動生成されたメソッド・スタブ
		String loginId = request.getParameter("login_id");
		String password = request.getParameter("password");

		if (StringUtils.isBlank(loginId)) {
			messages.add("ログインIDを入力してください");
		} else if (!loginId.matches("[a-zA-Z0-9]{6,20}")) {
			messages.add("ログインIDは半角英数字で6文字以上20文字以下で入力してください");
		}
		if (StringUtils.isBlank(password)) {
			messages.add("パスワードを入力してください");
		} else if (!password.matches("[ -/:-@\\[-~]{6,20}")) {
			messages.add("パスワードは半角英数字、記号で6文字以上20文字以下で入力してください");
		}
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}
