package bulletinBoard.dao;

import static chapter6.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import bulletinBoard.beans.ComplementUser;
import bulletinBoard.beans.User;
import bulletinBoard.exception.NoRowsUpdatedRuntimeException;
import bulletinBoard.exception.SQLRuntimeException;

public class UserDao {

	//ログイン時にユーザー情報のlogin_idとpasswordを指定
	public User getUser(Connection connection, String loginId,
			String password) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE (login_id = ?) AND password = ?";

			ps = connection.prepareStatement(sql);
			ps.setString(1, loginId);
			ps.setString(2, password);

			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);
			if (userList.isEmpty() == true) {
				return null;
			} else if (2 <= userList.size()) {
				throw new IllegalStateException("2 <= userList.size()");
			} else {
				return userList.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	//EditUser時にユーザー情報のlogin_idを指定 ※passwordは必要ない
	public User getUser(Connection connection, int id) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE id = ?";

			ps = connection.prepareStatement(sql);
			ps.setInt(1, id);

			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);
			if (userList.isEmpty() == true) {
				return null;
			} else if (2 <= userList.size()) {
				throw new IllegalStateException("2 <= userList.size()");
			} else {
				return userList.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	//UserManagementでユーザーのListを所得する値を指定
	public List<ComplementUser> getAiiComplementUser(Connection connection) {
		// TODO 自動生成されたメソッド・スタブ
		PreparedStatement ps = null;
		try {
			String sql = "";
			sql += "SELECT";
			sql += "   u.*";
			sql += " , b.name AS branch_name";
			sql += " , d.name AS department_name";
			sql += " FROM";
			sql += "   users AS u";
			sql += " LEFT JOIN";
			sql += "   branches AS b";
			sql += " ON";
			sql += "   u.branch_id = b.id";
			sql += " LEFT JOIN";
			sql += "   departments AS d";
			sql += " ON";
			sql += "   u.department_id = d.id";
			ps = connection.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			List<ComplementUser> userList = toComplementUserList(rs);
			if (userList.isEmpty()) {
				return null;
			} else {
				return userList;
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	//getUserで指定された要素をList化し、beans.User格納
	private List<User> toUserList(ResultSet rs) throws SQLException {

		List<User> ret = new ArrayList<User>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				int branchId = rs.getInt("branch_id");
				String password = rs.getString("password");
				int departmentId = rs.getInt("department_id");
				boolean isStopped = rs.getBoolean("is_stopped");
				Timestamp createdDate = rs.getTimestamp("created_date");
				Timestamp updatedDate = rs.getTimestamp("updated_date");

				User user = new User();
				user.setId(id);
				user.setLoginId(loginId);
				user.setName(name);
				user.setBranchId(branchId);
				user.setPassword(password);
				user.setDepartmentId(departmentId);
				user.setStopped(isStopped);
				user.setCreatedDate(createdDate);
				user.setUpdatedDate(updatedDate);
				ret.add(user);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

	//ComplementUserの指定されたbeans.ComplementUserに格納
	private List<ComplementUser> toComplementUserList(ResultSet rs) throws SQLException {
		// TODO 自動生成されたメソッド・スタブ
		List<ComplementUser> ret = new ArrayList<ComplementUser>();
		try {
			while (rs.next()) {
				ComplementUser user = new ComplementUser();
				user.setId(rs.getInt("id"));
				user.setLoginId(rs.getString("login_id"));
				user.setPassword(rs.getString("password"));
				user.setName(rs.getString("name"));
				user.setBranchId(rs.getInt("branch_id"));
				user.setDepartmentId(rs.getInt("department_id"));
				user.setStopped(rs.getBoolean("is_stopped"));
				user.setBranchName(rs.getString("branch_name"));
				user.setDepartmentName(rs.getString("department_name"));
				user.setName(rs.getString("name"));
				user.setCreatedDate(rs.getTimestamp("created_date"));
				user.setUpdateDate(rs.getTimestamp("updated_date"));
				ret.add(user);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

	//ユーザーの情報をDBに登録
	public void insert(Connection connection, User user) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO users ( ");
			sql.append("login_id");
			sql.append(", name");
			sql.append(", branch_id");
			sql.append(", password");
			sql.append(", department_id");
			sql.append(", is_stopped");
			sql.append(", created_date");
			sql.append(", updated_date");
			sql.append(") VALUES (");
			sql.append("?"); // login_id
			sql.append(", ?"); // name
			sql.append(", ?"); // branch_id
			sql.append(", ?"); // password
			sql.append(", ?"); // department_id
			sql.append(", ?"); //is_stopped
			sql.append(", CURRENT_TIMESTAMP"); // created_date
			sql.append(", CURRENT_TIMESTAMP"); // updated_date
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, user.getLoginId());
			ps.setString(2, user.getName());
			ps.setInt(3, user.getBranchId());
			ps.setString(4, user.getPassword());
			ps.setInt(5, user.getDepartmentId());
			ps.setBoolean(6, user.isStopped());
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	//EditUserでユーザー情報の更新
	public void update(Connection connection, User user) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			int idIndex = 5;
			sql.append("UPDATE users SET");
			sql.append("  login_id = ?");
			sql.append(", name = ?");
			sql.append(", branch_id = ?");
			sql.append(", department_id = ?");

			if (!user.getPassword().equals("")) {
				sql.append(", password = ?");
				idIndex = 6;
			}

			sql.append(" where id = ?");
			ps = connection.prepareStatement(sql.toString());
			ps.setString(1, user.getLoginId());
			ps.setString(2, user.getName());
			ps.setInt(3, user.getBranchId());
			ps.setInt(4, user.getDepartmentId());
			if (!user.getPassword().equals("")) {
				ps.setString(5, user.getPassword());
			}

			ps.setInt(idIndex, user.getId());
			ps.executeUpdate();
			int count = ps.executeUpdate();
			if (count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}


	public void setStatus(Connection connection, User user) {
		// TODO 自動生成されたメソッド・スタブ
		PreparedStatement ps = null;
		try {
			String sql = "UPDATE users SET is_stopped = ? WHERE id = ?";
			ps = connection.prepareStatement(sql);
			ps.setBoolean(1, !user.isStopped());
			ps.setInt(2, user.getId());
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}

	}

	public List<Integer> getUserIdList(Connection connection, int branchId) {
		// TODO 自動生成されたメソッド・スタブ
		PreparedStatement ps = null;
		try {
			String sql = "SELECT id FROM users WHERE branch_id = ? AND department_id = ?";
			ps = connection.prepareStatement(sql);
			ps.setInt(1, branchId);
			ps.setInt(2, 4);
			ResultSet rs = ps.executeQuery();
			List<Integer> userIdList = toUserIdList(rs);
			return userIdList;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<Integer> toUserIdList(ResultSet rs) throws SQLException {
		// TODO 自動生成されたメソッド・スタブ
		List<Integer> ret = new ArrayList<Integer>();
		try {
			while (rs.next()) {
				ret.add(rs.getInt("id"));
			}
			return ret;
		} finally {
			close(rs);
		}
	}



	public boolean duplicateCheck(Connection connection, User user) {
		// TODO 自動生成されたメソッド・スタブ
		PreparedStatement ps = null;
		try {
			String sql = "SELECT id FROM users WHERE login_id = ?";
			ps = connection.prepareStatement(sql);
			ps.setString(1, user.getLoginId());
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				return false;
			}
			return true;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}



}