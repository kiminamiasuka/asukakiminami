package bulletinBoard.dao;

import static chapter6.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import bulletinBoard.beans.CustomPost;
import bulletinBoard.beans.Post;
import bulletinBoard.beans.Search;
import bulletinBoard.exception.SQLRuntimeException;

public class PostDao { //クラスの宣言

	//Post内容をDBに登録
	public void insert(Connection connection, Post post) {
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO posts ( ");
			sql.append("user_id");
			sql.append(", subject");
			sql.append(", text");
			sql.append(", category");
			sql.append(", created_date");
			sql.append(", updated_date");
			sql.append(") VALUES (");
			sql.append(" ?"); // user_id
			sql.append(", ?"); // subject
			sql.append(", ?"); // text
			sql.append(", ?"); //category
			sql.append(", CURRENT_TIMESTAMP"); // created_date
			sql.append(", CURRENT_TIMESTAMP"); // updated_date
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());
			ps.setInt(1, post.getUserId());
			ps.setString(2, post.getSubject());
			ps.setString(3, post.getText());
			ps.setString(4, post.getCategory());

			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	//List<Post>にすべての要素を指定
	public List<Post> findAll(Connection connection) {
		PreparedStatement ps = null;
		try {
			String sql = "select * from posts";
			ps = connection.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			List<Post> postList = toPostList(rs);
			return postList;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	//beans.CustomPostに格納する要素を指定
	public CustomPost getCustomPost(Connection connection, int postsId) {
		// TODO 自動生成されたメソッド・スタブ
		PreparedStatement ps = null;
		try {
			String sql = "";
			sql += "SELECT";
			sql += "  p.*";
			sql += ", u.name";
			sql += " FROM";
			sql += "  posts AS p";
			sql += " LEFT JOIN";
			sql += "  users AS u";
			sql += " ON p.user_id = u.id";
			sql += " WHERE p.id = ?";
			ps = connection.prepareStatement(sql);
			ps.setInt(1, postsId);
			ResultSet rs = ps.executeQuery();
			List<CustomPost> customPostList = toCustomPostList(rs);
			if (customPostList.isEmpty()) {
				return null;
			} else if (2 <= customPostList.size()) {
				throw new IllegalStateException();
			} else {
				return customPostList.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}

	}

	//投稿のすべてを表示する要素を指定
	public List<CustomPost> customPostAll(Connection connection) {
		// TODO 自動生成されたメソッド・スタブ
		PreparedStatement ps = null;
		try {
			String sql = "";
			sql += "SELECT";
			sql += "  p.*";
			sql += ", u.name";
			sql += ", (SELECT COUNT(*) FROM comments WHERE posts_id = p.id) AS comment_count";
			sql += " FROM";
			sql += "  posts AS p";
			sql += " LEFT JOIN";
			sql += "  users AS u";
			sql += " ON p.user_id = u.id";
			sql += " ORDER BY p.updated_date DESC";
			ps = connection.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			List<CustomPost> customPostList = toCustomPostList(rs);
			return customPostList;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	//List<Post>に指定された要素を格納
	private List<Post> toPostList(ResultSet rs) throws SQLException {
		// TODO 自動生成されたメソッド・スタブ
		List<Post> ret = new ArrayList<Post>();
		try {
			while (rs.next()) {
				Post post = new Post();
				post.setId(rs.getInt("id"));
				post.setUserId(rs.getInt("user_id"));
				post.setSubject(rs.getString("subject"));
				post.setText(rs.getString("text"));
				post.setCategory(rs.getString("category"));
				post.setCreatedDate(rs.getDate("created_date"));
				post.setUpdatedDate(rs.getDate("updated_date"));
				ret.add(post);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

	//beans.CustomPostに指定された要素を格納＆検索で絞り込まれた要素の格納
	private List<CustomPost> toCustomPostList(ResultSet rs) throws SQLException {
		// TODO 自動生成されたメソッド・スタブ
		List<CustomPost> ret = new ArrayList<CustomPost>();
		try {
			while (rs.next()) {
				CustomPost customPost = new CustomPost();
				customPost.setId(rs.getInt("id"));
				customPost.setUserId(rs.getInt("user_id"));
				customPost.setSubject(rs.getString("subject"));
				customPost.setText(rs.getString("text"));
				customPost.setCategory(rs.getString("category"));
				customPost.setName(rs.getString("name"));
				customPost.setCommentCount(rs.getString("comment_count"));
				customPost.setCreatedDate(rs.getDate("created_date"));
				customPost.setUpdatedDate(rs.getDate("updated_date"));
				ret.add(customPost);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

	//検索内容の指定と格納する要素の指定
	public List<CustomPost> search(Connection connection, Search search) {
		PreparedStatement ps = null;
		try {
			String sql = "";
			sql += "SELECT";
			sql += " p.*";
			sql += " , u.name";
			sql += ",(SELECT COUNT(*) FROM comments WHERE posts_id = p.id) AS comment_count";
			sql += " FROM";
			sql += "  posts AS p";
			sql += " LEFT JOIN";
			sql += "  users AS u";
			sql += " ON";
			sql += "  p.user_id = u.id";
			sql += " WHERE";
			sql += "  p.id IN";
			sql += " (";
			sql += "	SELECT";
			sql += "	  id";
			sql += "	FROM";
			sql += "	  posts";
			sql += "	WHERE";
			sql += "	  updated_date BETWEEN";

			int count = 1;
			boolean fromFlag = false;
			//開始日
			if (search.getFrom().equals("")) { //equals = 参照先の値を比較
				sql += " (SELECT updated_date FROM posts ORDER BY updated_date LIMIT 1)";
			} else {
				sql += " ?";
				count += 1;
				fromFlag = true;
			}
			sql += " AND";
			//終了日
			if (search.getTo().equals("")) {
				sql += " (SELECT updated_date FROM posts ORDER BY updated_date DESC LIMIT 1)";
			} else {
				sql += " ?";
				count += 1;
			}
			sql += " )";
			sql += " AND";
			//カテゴリー
			sql += "  p.category LIKE ?";
			sql += " ORDER BY p.updated_date DESC";
			ps = connection.prepareStatement(sql);
			if (count == 2) {
				if (fromFlag)
					ps.setString(1, search.getFrom());
				if (!fromFlag)
					ps.setString(1, search.getTo() + " 23:59:59");
			}
			if (count == 3) {
				ps.setString(1, search.getFrom());
				ps.setString(2, search.getTo() + " 23:59:59");
			}
			ps.setString(count, "%" + search.getCategory() + "%");
			ResultSet rs = ps.executeQuery();
			List<CustomPost> customPostList = toCustomPostList(rs);
			if (customPostList.isEmpty()) {
				return null;
			} else {
				return customPostList;
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	//deletePostで指定されたPostsテーブルのidをDELETE
	public void delete(Connection connection, int id) {
		// TODO 自動生成されたメソッド・スタブ
		PreparedStatement ps = null;
		try {
			String sql = "DELETE FROM posts WHERE id = ?";
			ps = connection.prepareStatement(sql);
			ps.setInt(1, id);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}

	}

}