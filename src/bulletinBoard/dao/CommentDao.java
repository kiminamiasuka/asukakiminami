package bulletinBoard.dao;

import static chapter6.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import bulletinBoard.beans.Comment;
import bulletinBoard.exception.SQLRuntimeException;

public class CommentDao {
	//コメント機能で使用
	public void insert(Connection connection, Comment comment) { //insertを使うメソッドを指定
		//対象文字列.insert(追加位置,追加対象 )   insert = 文字列を任意の場所に追加
		PreparedStatement ps = null; //初期化     PreparedStatement = データベースに対するSQL文を実行するために使用
		try { // StringBuilder = 指定した位置に文字列を挿入
			StringBuilder sql = new StringBuilder(); //StringBuilderの初期化と変数宣言
			sql.append("INSERT INTO comments ( "); //INSERT INTO messagesをappendでsqlに代入
			sql.append("user_id"); //.append = 指定した子要素の最後にテキスト文字やHTML要素を追加することができるメソッド
			sql.append(", posts_id");
			sql.append(", user_comment");
			sql.append(", created_date");
			sql.append(", updated_date");
			sql.append(") VALUES (");
			sql.append(" ?"); // user_id
			sql.append(", ?"); // post_id
			sql.append(", ?"); // user_comment
			sql.append(", NOW()"); // created_date
			sql.append(", NOW()"); // updated_date
			sql.append(")");
			//.toString()=数値型などをString型の文字列に変換  .prepareStatement=文をデータベースに送る
			ps = connection.prepareStatement(sql.toString()); //sqlを文字列に変換しデータベースに送る
			//connection=特定のデータベースとの接続
			ps.setInt(1, comment.getUserId()); //messageから値をgetで取りpsにsetする
			ps.setInt(2, comment.getPostsId());
			ps.setString(3, comment.getComment());

			ps.executeUpdate(); //.executeUpdate()=前処理済みのSQL文を実行し，更新行数を返却します。
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	//Commentのdelete用Dao
	public void delete(Connection connection, int id) {
		PreparedStatement ps = null;
		try {
			String sql = "DELETE FROM comments WHERE id = ?";
			ps = connection.prepareStatement(sql);
			ps.setInt(1, id);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	//コメントのすべてを表示するときに使用
	public List<Comment> commentAll(Connection connection) {
		// TODO 自動生成されたメソッド・スタブ
		PreparedStatement ps = null;
		try {
			String sql = "";
			sql += "SELECT";
			sql += "  c.*";
			sql += ", u.name";
			sql += ", (SELECT COUNT(*) FROM comments WHERE posts_id = p.id) AS comment_count";
			sql += " FROM";
			sql += "  comments AS c";
			sql += " LEFT JOIN";
			sql += "  users AS u";
			sql += " ON c.user_id = u.id";
			sql += " LEFT JOIN";
			sql += "  posts AS p";
			sql += " ON c.posts_id = p.id";
			sql += " ORDER BY c.updated_date DESC";
			ps = connection.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			List<Comment> commentList = toCommentList(rs);
			return commentList;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}finally {
			close(ps);
		}
	}

	//List化されたコメントの情報をbeans.commentに格納
	private List<Comment> toCommentList(ResultSet rs) throws SQLException {
		// TODO 自動生成されたメソッド・スタブ
		List<Comment> ret = new ArrayList<Comment>();
		try {
			while(rs.next()) {
				Comment comment = new Comment();
				comment.setId(rs.getInt("id"));
				comment.setUserId(rs.getInt("user_id"));
				comment.setPostsId(rs.getInt("posts_id"));
				comment.setName(rs.getString("name"));
				comment.setComment(rs.getString("user_comment"));
				comment.setCreatedDate(rs.getDate("created_date"));
				comment.setUpdatedDate(rs.getDate("updated_date"));
				ret.add(comment);
			}
			return ret;
		}finally {
			close(rs);
		}
	}
}
