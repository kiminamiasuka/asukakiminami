<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>掲示板</title>
<link href="./css/style.css" rel="stylesheet" type="text/css">
<link rel="stylesheet"
	href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">


</head>
<body style="background: #ff000; text-align: center;">

	<div class="container">
		<div class="row">

			<!-- エラー確認 -->
			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul class="error">
						<c:forEach items="${errorMessages}" var="message">
							<li class="validate"><c:out value="${message}" /></li>
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session" />
			</c:if>
			<h1>掲示板</h1>

			<h2>
				<c:out value="${loginUser.name}" />
			</h2>

			<!-- ナヴィ機能＆管理リンクの表示の有無	 -->
			<div id="navi">
				<a href="./">ホーム</a> <a href="post">新規投稿</a>
				<c:if test="${ not empty isManager }">
					<a href="userManagement">ユーザー管理</a>
				</c:if>
				<a href="logout">ログアウト</a>
			</div>

			<!-- 検索機能 -->
			<div id="search">
				<form action="./" method="get" role="search" class="form-inline" id="search">

					<br />
					<!-- カテゴリー検索 -->
					<div class="form-group">
						<label for="search_category">カテゴリー:<input name="category"
							id="search_category" type="text" class="form-control"
							value="${editCategory}" placeholder="カテゴリー" /></label>
					</div>

					<!-- 日時検索 -->
					<!-- 開始日指定 -->
					<div class="form-group">
						<label for="from">開始日:<input name="from" id="from"
							type="date" class="form-control" value="${editFrom}"
							placeholder="開始日" /></label>
					</div>
					<!-- 終了日指定 -->
					<div class="form-group">
						<label for="to">終了日:<input name="to" id="to" type="date"
							class="form-control" value="${editTo}" placeholder="終了日" /></label>
					</div>

					<button type="submit" class="btn btn-default">検索</button>
				</form>
			</div>
			<br />

			<div class="profile">


				<!-- 投稿表示 -->

				<c:forEach items="${customPostList}" var="customPost">
					<div class="customPost">
						<div
							style="padding: 10px; margin-bottom: 10px; border: 5px double #333333; border-radius: 10px; background-color: #ffff99;">
							件名：
							<c:out value="${customPost.subject}" />
							<br /> 本文：
							<c:out value="${customPost.text}" />
							<br /> カテゴリー：
							<c:out value="${customPost.category}" />
							<br /> 投稿者：
							<c:out value="${customPost.name}" />

							<!-- 削除 -->
							<div class="form-group">
								<c:if test="${customPost.userId == loginUser.id }">
									<form action="deletePost" method="post" class="form-horizontal">
										<input type="submit" value="投稿削除"
											class="btn btn-danger btn-xs"
											onclick="return confirm('削除してもよろしいですか？');" /> <input
											name="id" type="hidden"
											value="<c:out value="${customPost.id}" />" />
									</form>
								</c:if>
							</div>
						</div>

						<div
							style="padding: 10px; margin-bottom: 10px; border: 5px double #333333; border-radius: 10px;">
							<!-- コメント記入 -->
							<div class="form-group">
								<div class="col-sm-3 col-sm-pull-9">
									<form action="./" method="post" class="form-horizontal">
										<div class="form-group">
											<label for="comment" class="control-label">コメント</label>
											<textarea id="comment" name="comment" rows="7" cols="40"wrap="hard"
												class="form-control" placeholder="500文字以下で入力してください" /></textarea>
										</div>
										<input type="hidden" name="posts_id"
											value="<c:out value="${customPost.id}" />" /> <input
											type="hidden" name="user_id"
											value="<c:out value="${loginUser.id}" />" />
										<div class="form-group">
											<input type="submit" value="コメントする" class="btn btn-default" />
										</div>
									</form>
								</div>
							</div>

							<!-- コメント表示 -->

							<c:forEach items="${commentList}" var="comment">
								<div class="comment">
									<c:if test="${customPost.id == comment.postsId }">
									コメント：<c:out value="${comment.comment}" />
										<br />
									返信者：<c:out value="${comment.name}" />
										<!-- コメント削除 -->
										<div class="form-group">
											<c:if test="${comment.userId ==  loginUser.id }">
												<form action="deleteComment" method="post"
													class="form-horizontal">
													<input type="submit" value="コメント削除"
														class="btn btn-danger btn-xs"
														onclick="return confirm('削除してもよろしいですか？');" /><input
														name="id" type="hidden"
														value="<c:out value="${comment.id}" />" />
												</form>
											</c:if>
										</div>
									</c:if>
								</div>
							</c:forEach>
						</div>
						<a class="post"><c:out value="${customPost.updatedDate}" /></a>
					</div>
				</c:forEach>
				<div class="copylight">Copyright(c)AsukaKiminami</div>
			</div>

		</div>
</body>
</html>
