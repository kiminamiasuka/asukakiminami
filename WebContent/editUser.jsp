<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${editUser.name}の編集</title>
<link href="css/style.css" rel="stylesheet" type="text/css">
<link rel="stylesheet"
	href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<link rel="stylesheet"
	href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
</head>
<body style="background: #ff000; text-align: center;">
	<div class="container">
		<div class="row">

			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul class="error">
						<c:forEach items="${errorMessages}" var="message">
							<li class="validate"><span class="glyphicon glyphicon-ok"></span>
								${message}</li>
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session" />
			</c:if>

			<div id="navi">
				<a href="./">ホーム</a> <a href="userManagement">ユーザー管理</a>
				<p class="logout">
					<a href="logout">ログアウト</a>
				</p>
			</div>
			<div class="panel panel-default center-block"
				style="margin-top: 20px;">
				<div class="panel-heading">ユーザー編集</div>
				<div class="panel-body">
					<form action="editUser" method="post" class="form-horizontal">
						<div class="form-group">
							<label for="login_id" class="control-label col-sm-2">ログインID</label>
							<div class="col-sm-8">
								<input id="login_id" name="login_id" value="${editUser.loginId}"
									class="form-control" placeholder="半角英数字 6文字以上20文字以下で入力してください" />
							</div>
						</div>
						<div class="form-group">
							<label for="password" class="control-label col-sm-2">パスワード</label>
							<div class="col-sm-8">
								<input id="password" name="password" type="password"
									class="form-control" placeholder="6文字以上で入力してください" />
							</div>
						</div>
						<div class="form-group">
							<label for="password_confirm" class="control-label col-sm-2">パスワード(確認用)</label>
							<div class="col-sm-8">
								<input id="password_confirm" name="password_confirm"
									type="password" class="form-control" />
							</div>
						</div>

						<div class="form-group">
							<label for="name" class="control-label col-sm-2">名前</label>
							<div class="col-sm-8">
								<input id="name" name="name" value="${editUser.name}"
									class="form-control" placeholder="6文字以上で入力してください" />
							</div>
						</div>

						<div class="form-group">
							<c:if test="${myId != userId}">
								<label for="branch_id" class="control-label col-sm-2">支店名</label>
								<div class="col-sm-8">
									<select name="branch_id" class="form-control">
										<c:forEach items="${branch}" var="branch">
											<option value="${branch.key}"
												<c:if test="${editUser.branchId == branch.key}">selected</c:if>>${branch.value}</option>
										</c:forEach>
									</select>
								</div>
							</c:if>
						</div>

						<div class="form-group">
							<c:if test="${myId != userId}">
								<label for="department_id" class="control-label col-sm-2">部署・役職</label>
								<div class="col-sm-8">
									<select name="department_id" class="form-control">
										<c:forEach items="${department}" var="department">
											<option value="${department.key}"
												<c:if test="${editUser.departmentId == department.key}">selected</c:if>>${department.value}</option>
										</c:forEach>
									</select>
								</div>
							</c:if>
						</div>

						<input type="hidden" name="id" value="${userId}" />
						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-4">
								<input type="submit" value="登録" class="btn btn-default" />
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<div class="copylight">Copyright(c)AsukaKiminami</div>
</body>
</html>